#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "trackload.h"
#include "video.h"

#define MIN(x,y) (((x) > (y)) ? (y) : (x))
#define MAX(x,y) (((x) < (y)) ? (y) : (x))

#define TRACKFETCH8() ({ \
	uint8_t ret = *(uint8_t*)((uint8_t*)trackdata + idx); \
	idx += 1; \
	ret; \
})
#define TRACKFETCH16() ({ \
	uint16_t ret; \
	uint16_t val = *(uint16_t*)((uint8_t*)trackdata + idx); \
	ret  = (val & 0xFF00) >> 8; \
	ret |= (val & 0x00FF) << 8; \
	idx += 2; \
	ret; \
})
#define TRACKFETCH32() ({ \
	uint32_t ret; \
	uint32_t val = *(uint32_t*)((uint8_t*)trackdata + idx); \
	ret  = (val & 0xFF000000) >> 24; \
	ret |= (val & 0x00FF0000) >> 8; \
	ret |= (val & 0x0000FF00) << 8; \
	ret |= (val & 0x000000FF) << 24; \
	idx += 4; \
	ret; \
})

void track_load(void* trackdata, Track* track)
{
	uint32_t idx = 0, nextidx;
	uint32_t headsz;
	uint32_t eventsz;
	uint32_t i, j;
	i = TRACKFETCH32();
	if(i != 0xBADCAFE) {
		return;
	}
	headsz = TRACKFETCH32();
	track->event_count = TRACKFETCH32();
	eventsz = TRACKFETCH32();
	track->roadcolors[0] = TRACKFETCH32();
	track->roadcolors[1] = TRACKFETCH32();
	track->outcolors[0] = TRACKFETCH32();
	track->outcolors[1] = TRACKFETCH32();
	track->edgecolors[0] = TRACKFETCH32();
	track->edgecolors[1] = TRACKFETCH32();
	track->lanecolors[0] = TRACKFETCH32();
	track->lanecolors[1] = TRACKFETCH32();
	idx = headsz;
	for(i = 0; i < track->event_count; i++) {
		nextidx = idx + eventsz;
		j = TRACKFETCH32();
		if(j != 0x45564E54) { /* 'EVNT' */
			track->events[i].enable = 0;
			idx = nextidx;
			continue;
		}
		track->events[i].enable = 1;
		track->events[i].dist = (float)TRACKFETCH32() / 65536.0f;
		track->events[i].flags = TRACKFETCH32();
		idx += 4;
		for(j = 0; j < 4; j++) {
			track->events[i].params[j] = TRACKFETCH32();
		}
		idx = nextidx;
	}
}

static void invoke_track_event(Roadgen* road, Track* track, int ev)
{
	Trackevent* e;
	e = &(track->events[ev]);
	if(e->flags & 1) { /* Curve */
#define CURVE_MINTIME 1.0f
		if(e->flags & 1 << 31) {
			if(road->speed != 0)
				road->road_motions[e->params[0]].turn_pitch += e->fparams[3] * e->fparams[1] / ((SPEED_MAX - road->speed) * (SPEED_MAX - road->speed) * CURVE_MINTIME);
			e->intr_progress++;
			int cond;
			if((e->params[1] ^ e->params[3]) & (1 << 31))
				cond = road->road_motions[e->params[0]].turn_pitch <= e->target;
			else
				cond = road->road_motions[e->params[0]].turn_pitch >= e->target;
			if(cond) {
				e->enable = 0;
				e->flags &= ~(1 << 31);
			}
		}else{
			e->fparams[1] = ((float)e->params[1]) / 65536.0f;
			e->fparams[3] = ((float)e->params[3]) / 65536.0f;
			road->road_motions[e->params[0]].active = 1;
			if(!e->params[2]) {
				road->road_motions[e->params[0]].segment = ROADLINES;
				road->road_motions[e->params[0]].endsegment = ROADLINES+1;
				e->target = e->fparams[1];
			}else{
				e->target = road->road_motions[e->params[0]].turn_pitch + e->fparams[1];
			}
			e->intr_progress = 0;
			e->flags |= 1 << 31;
		}
	}else if(e->flags & 2) { /* Hill */
//#if 0
		if(e->flags & 1 << 31) {
			int i, j, ea;
			for(i = 0, j = e->intr_progress; i < MIN(e->intr_progress, ROADLINES); i++, j--) {
				ea = ROADLINES - i - 1;
				switch(e->params[2]) {
					case 0: /* Linear */
						road->vstretch[ea] = e->fparams[1] *
							e->fparams[3] * (j + 1);
						break;
					case 1: /* Logarithmic (base is param[3]) */
						road->vstretch[ea] = log10(e->fparams[1] *
							 (j + 1)) / log10(e->fparams[3]);
						break;
					case 2: /* Exponential (base is param[3]) */
						road->vstretch[ea] = pow(e->fparams[3],
							e->fparams[1] * (j + 1));
						break;
					case 3: /* Reverse Exponential (exponent is param[3]) */
						road->vstretch[ea] = pow(e->fparams[1] * (j + 1),
							e->fparams[3]);
						break;
					default:
						road->vstretch[ea] = 1.0f;
						break;
				}
			}
			e->intr_progress++;
			if(road->position > e->target) {
				e->enable = 0;
				e->flags &= ~(1 << 31);
			}
		}else{
			e->fparams[0] = ((float)e->params[0]) / 65536.0f;
			e->fparams[1] = ((float)e->params[1]) / 65536.0f;
			e->fparams[3] = ((float)e->params[3]) / 65536.0f;
			e->target = road->position + e->fparams[0];
			e->intr_progress = 0;
			e->flags |= 1 << 31;
		}
//#endif
	}else{
		e->enable = 0;
	}
}

void track_update_road(Roadgen* road, Track* track)
{
	int i;
	for(i = 0; i < track->event_count; i++) {
		if(!track->events[i].enable)
			continue;
		if(road->position >= track->events[i].dist) {
			invoke_track_event(road, track, i);
		}
	}
}

void track_register(Track track, Roadgen* road)
{
	road->roadcolors[0] = track.roadcolors[0];
	road->roadcolors[1] = track.roadcolors[1];
	road->outcolors[0] = track.outcolors[0];
	road->outcolors[1] = track.outcolors[1];
	road->edgecolors[0] = track.edgecolors[0];
	road->edgecolors[1] = track.edgecolors[1];
	road->lanecolors[0] = track.lanecolors[0];
	road->lanecolors[1] = track.lanecolors[1];
}


