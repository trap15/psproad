#include <pspge.h>
#include <pspgu.h>

#include "video.h"

void* fbp0;
void* fbp1;
void* zbp;

/* The VRAM allocation stuff is taken from the PSPSDK examples. */

unsigned int __attribute__((aligned(16))) gulist[262144];

static unsigned int staticOffset = 0;
static unsigned int getMemorySize(unsigned int width, unsigned int height, unsigned int psm)
{
	switch (psm)
	{
		case GU_PSM_T4:
			return (width * height) >> 1;

		case GU_PSM_T8:
			return width * height;

		case GU_PSM_5650:
		case GU_PSM_5551:
		case GU_PSM_4444:
		case GU_PSM_T16:
			return 2 * width * height;

		case GU_PSM_8888:
		case GU_PSM_T32:
			return 4 * width * height;

		default:
			return 0;
	}
}

void* getStaticVramBuffer(unsigned int width, unsigned int height, unsigned int psm)
{
	unsigned int memSize = getMemorySize(width,height,psm);
	void* result = (void*)staticOffset;
	staticOffset += memSize;

	return result;
}

void* getStaticVramTexture(unsigned int width, unsigned int height, unsigned int psm)
{
	void* result = getStaticVramBuffer(width,height,psm);
	return (void*)(((unsigned int)result) + ((unsigned int)sceGeEdramGetAddr()));
}

/* Start original code */

void drawSprites(int count, Vertex* vtxs)
{
	sceGumDrawArray(GU_TRIANGLE_STRIP, 
			GU_COLOR_8888 | GU_VERTEX_32BITF,
			count, 0, vtxs);
}

void drawQuads(int count, quadVertex* vtxs)
{
	int i, z;
	Vertex* vertices = sceGuGetMemory(count * sizeof(Vertex) * 2);
	for(i = 0, z = 0; i < count; i++) {
		vertices[z].color = vtxs[i].color;
		vertices[z].pos = vtxs[i].tl;
		z++;
		vertices[z].color = vtxs[i].color;
		vertices[z].pos = vtxs[i].br;
		z++;
	}
	sceGumDrawArray(GU_SPRITES, 
			GU_COLOR_8888 | GU_VERTEX_32BITF,
			count * 2, 0, vertices);
}

void initVideo(void)
{
	fbp0 = getStaticVramBuffer(BUF_WIDTH,SCR_HEIGHT,GU_PSM_8888);
	fbp1 = getStaticVramBuffer(BUF_WIDTH,SCR_HEIGHT,GU_PSM_8888);
	zbp = getStaticVramBuffer(BUF_WIDTH,SCR_HEIGHT,GU_PSM_4444);

	sceGuInit();

	sceGuStart(GU_DIRECT,gulist);
	sceGuDrawBuffer(GU_PSM_8888,fbp0,BUF_WIDTH);
	sceGuDispBuffer(SCR_WIDTH,SCR_HEIGHT,fbp1,BUF_WIDTH);
	sceGuDepthBuffer(zbp,BUF_WIDTH);
	sceGuOffset(2048 - (SCR_WIDTH/2),2048 - (SCR_HEIGHT/2));
	sceGuViewport(2048,2048,SCR_WIDTH,SCR_HEIGHT);
	sceGuDepthRange(65535,0);
	sceGuDepthFunc(GU_GREATER);
	sceGuEnable(GU_DEPTH_TEST);
	sceGuScissor(0,0,SCR_WIDTH,SCR_HEIGHT);
	sceGuEnable(GU_SCISSOR_TEST);
	sceGuFrontFace(GU_CW);
	sceGuShadeModel(GU_SMOOTH);
	sceGuDisable(GU_TEXTURE_2D);
	sceGuFinish();
	sceGuSync(0,0);

	sceDisplayWaitVblankStart();
	sceGuDisplay(1);
}

