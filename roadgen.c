#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "video.h"
#include "roadgen.h"

#define ROAD_Y			-1
#define PLR_LINE		32
#define START_HWIDTH		128.0f
#define START_EWIDTH		16.0f
#define START_LWIDTH		12.0f
#define START_LANEOFF		64.0f
#define SEGMENT_STEP		1
#define VERTEX_MAX		ROADLINES

/* Background color */
quadVertex road_vtxs[VERTEX_MAX];
quadVertex grass_vtxs[VERTEX_MAX];
quadVertex edge1_vtxs[VERTEX_MAX];
quadVertex edge2_vtxs[VERTEX_MAX];
quadVertex lane1_vtxs[VERTEX_MAX];
quadVertex lane2_vtxs[VERTEX_MAX];

void roadgen_init(Roadgen* road)
{
	int i;
	for(i = 0; i < ROADLINES; i++) {
		road->zmap[i] = (float)((float)ROAD_Y / (float)(i - (SCR_HEIGHT/2))) * ((float)ROADLINES / (float)((float)ROAD_Y / (float)(PLR_LINE - (SCR_HEIGHT/2))));
		road->vstretch[i] = 1.0f;
	}
	for(i = 0; i < 8; i++) {
		road->road_motions[i].active = 0;
		road->road_motions[i].segment = 0;
		road->road_motions[i].endsegment = 0;
		road->road_motions[i].turn_pitch = 0.0;
	}
	road->outcolors[0] = 0xFF40A010;
	road->roadcolors[0] = 0xFF808080;
	road->lanecolors[0] = 0xFFC0C0C0;
	road->edgecolors[0] = 0xFFC0C0C0;
	road->outcolors[1] = 0xFF308008;
	road->roadcolors[1] = 0xFF606060;
	road->lanecolors[1] = 0xFF606060;
	road->edgecolors[1] = 0xFF1040A0;
	road->texoff = 100;
	road->position = 0;
	road->speed = 0.0f;
	road->xscroll = 0.0f;
}

#define GET_Z_VALUE(line, diff) ((((ROADLINES - line) - 1) / (ROADLINES + 48)) + (diff / 1000.0f))

void roadgen_draw(Roadgen* road, Roadraster_cb cb)
{
	int i;
	float roadwstep, edgewstep, lanewstep, laneostep;
	uint32_t grasscolor, roadcolor, lanecolor, edgecolor;
	float screenline = SCR_HEIGHT;

	road->x = SCR_WIDTH / 2 + road->xscroll;
	road->dx = 0;
	roadwstep = 1.0f;
	edgewstep = roadwstep * START_EWIDTH / START_HWIDTH;
	lanewstep = roadwstep * START_LWIDTH / START_HWIDTH;
	laneostep = roadwstep * START_LANEOFF / START_HWIDTH;

	road->roadwidth = START_HWIDTH;
	road->edgewidth = START_EWIDTH;
	road->lanewidth = START_LWIDTH;
	road->laneoff = START_LANEOFF;

	for(i = 0; i < ROADLINES; i++) {
		if((int)floor(road->zmap[i] + road->texoff) % ROADLINES > ROADLINES/2) {
			grasscolor = road->outcolors[0];
			roadcolor = road->roadcolors[0];
			lanecolor = road->lanecolors[0];
			edgecolor = road->edgecolors[0];
		}else{
			grasscolor = road->outcolors[1];
			roadcolor = road->roadcolors[1];
			lanecolor = road->lanecolors[1];
			edgecolor = road->edgecolors[1];
		}
		if(screenline >= 0) {
			road_vtxs[i].color = roadcolor;
			grass_vtxs[i].color = grasscolor;
			edge1_vtxs[i].color = edge2_vtxs[i].color = edgecolor;
			lane1_vtxs[i].color = lane2_vtxs[i].color = lanecolor;

			road_vtxs[i].tl.x = road->x + road->roadwidth;
			road_vtxs[i].br.x = road->x - road->roadwidth;
			grass_vtxs[i].tl.x = SCR_WIDTH;
			grass_vtxs[i].br.x = 0;
			edge1_vtxs[i].tl.x = road->x + road->roadwidth;
			edge1_vtxs[i].br.x = road->x + road->roadwidth + road->edgewidth;
			edge2_vtxs[i].tl.x = road->x - road->roadwidth;
			edge2_vtxs[i].br.x = road->x - road->roadwidth - road->edgewidth;
			lane1_vtxs[i].tl.x = road->x - road->roadwidth + road->laneoff;
			lane1_vtxs[i].br.x = road->x - road->roadwidth + road->laneoff + road->lanewidth;
			lane2_vtxs[i].tl.x = road->x + road->roadwidth - road->laneoff - road->lanewidth;
			lane2_vtxs[i].br.x = road->x + road->roadwidth - road->laneoff;

			road_vtxs[i].tl.y = grass_vtxs[i].tl.y =
			edge1_vtxs[i].tl.y = edge2_vtxs[i].tl.y =
			lane1_vtxs[i].tl.y = lane2_vtxs[i].tl.y = screenline - road->vstretch[i];
			road_vtxs[i].br.y = grass_vtxs[i].br.y =
			edge1_vtxs[i].br.y = edge2_vtxs[i].br.y =
			lane1_vtxs[i].br.y = lane2_vtxs[i].br.y = screenline;

			road_vtxs[i].tl.z = road_vtxs[i].br.z = GET_Z_VALUE(screenline, -1);
			grass_vtxs[i].tl.z = grass_vtxs[i].br.z = GET_Z_VALUE(screenline, -2);
			edge1_vtxs[i].tl.z = edge1_vtxs[i].br.z = 
			edge2_vtxs[i].tl.z = edge2_vtxs[i].br.z = 
			lane1_vtxs[i].tl.z = lane1_vtxs[i].br.z = 
			lane2_vtxs[i].tl.z = lane2_vtxs[i].br.z = GET_Z_VALUE(screenline, 0);
		}
		road->roadwidth -= roadwstep;
		road->edgewidth -= edgewstep;
		road->lanewidth -= lanewstep;
		road->laneoff -= laneostep;
		screenline -= road->vstretch[i];
		int j;
		for(j = 0; j < 8; j++) {
			if(!road->road_motions[j].active && (road->road_motions[j].endsegment <= 0))
				continue;
			if((i > road->road_motions[j].segment) && (i < road->road_motions[j].endsegment)) {
				road->dx += road->road_motions[j].turn_pitch;
			}
#define PUSH_DIV 8.0f
			road->xscroll += road->road_motions[j].turn_pitch * sqrt(road->speed) / PUSH_DIV;
			road->road_motions[j].segment -= road->speed / (float)ROADLINES;
			if(!road->road_motions[j].active)
				road->road_motions[j].endsegment -= road->speed / (float)ROADLINES;
		}
		road->x += road->dx - (road->xscroll / ROADLINES);
		if(cb != NULL)
			cb(road);
	}
	road->position += road->speed;
	road->texoff += road->speed;
	while(road->texoff > ROADLINES)
		road->texoff -= ROADLINES;

	drawQuads(VERTEX_MAX, road_vtxs);
	drawQuads(VERTEX_MAX, grass_vtxs);
	drawQuads(VERTEX_MAX, edge1_vtxs);
	drawQuads(VERTEX_MAX, edge2_vtxs);
	drawQuads(VERTEX_MAX, lane1_vtxs);
	drawQuads(VERTEX_MAX, lane2_vtxs);
}

