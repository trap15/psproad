#ifndef TRACKLOAD_H_
#define TRACKLOAD_H_

#include <stdint.h>
#include "roadgen.h"

typedef struct {
	float dist;
	uint32_t flags;
	int32_t params[4];
	float fparams[4];
	
	/* Event progress */
	int enable;
	int intr_progress;
	float target;
} Trackevent;

typedef struct {
	uint32_t roadcolors[2];
	uint32_t outcolors[2];
	uint32_t edgecolors[2];
	uint32_t lanecolors[2];
	uint32_t event_count;
	Trackevent *events;
} Track;

void track_load(void* trackdata, Track* track);
void track_update_road(Roadgen* road, Track* track);
void track_register(Track track, Roadgen* road);

#endif

