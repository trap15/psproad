#ifndef VIDEO_H_
#define VIDEO_H_

#include <pspgu.h>
#include <pspgum.h>

#define BUF_WIDTH (512)
#define SCR_WIDTH (480)
#define SCR_HEIGHT (272)

typedef struct {
	uint32_t color;
	ScePspFVector3 pos;
} __attribute__((aligned(16))) Vertex;

typedef struct {
	uint32_t color;
	ScePspFVector3 tl;
	ScePspFVector3 br;
} quadVertex;

extern void* fbp0;
extern void* fbp1;
extern void* zbp;

void* getStaticVramBuffer(unsigned int width, unsigned int height, unsigned int psm);
void* getStaticVramTexture(unsigned int width, unsigned int height, unsigned int psm);

void drawSprites(int count, Vertex* vtxs);
void drawQuads(int count, quadVertex* vtxs);

void initVideo(void);

#endif

