#!/usr/bin/env ruby

$HEADERSIZE = 0x30
$EVENTSIZE = 0x20

class EventTypes
	EVENT_CURVE		= 0x00000001
	EVENT_HILL		= 0x00000002
end

class GradientTypes
	GRADIENT_LINEAR		= 0x00000000
	GRADIENT_LOG		= 0x00000001
	GRADIENT_EXPONENT	= 0x00000002
	GRADIENT_REVEXPONENT	= 0x00000002
end

def float_to_fixed(num)
	return (num * 65536.0).floor
end

class TrackEvent
	attr_accessor :dist
	attr_reader :type
	attr_reader :param
	def initialize(type)
		@type = type
		@dist = 0.0
		@param = Array.new(4) {|i| 0}
	end
	def setParam(param, value)
		@param[param % 4] = value
	end
	def gradientLength(value)
		@param[0] = float_to_fixed(value)
	end
	def gradientSpeed(value)
		@param[1] = float_to_fixed(value)
	end
	def gradientType(type, value)
		@param[2] = type
		@param[3] = float_to_fixed(value)
	end
	def curveHandler(value)
		@param[0] = value
	end
	def curvePitch(value)
		@param[1] = float_to_fixed(value)
	end
	def curveSpeed(value)
		@param[3] = float_to_fixed(value)
	end
	def curveIsRelative(value)
		@param[2] = value
	end
	def writeToFile(outf)
# Magic
		outf.write([ 0x45564E54 ].pack("N"))
# Event Location
		outf.write([ float_to_fixed(@dist) ].pack("N"))
# Flags
		outf.write([ @type ].pack("N"))
# Padding
		outf.write([ 0 ].pack("N"))
# Write parameters
		@param.each {|x| outf.write([ x ].pack("N"))}
	end
end

class Track
	attr_accessor :roadcolor0, :roadcolor1
	attr_accessor :outcolor0, :outcolor1
	attr_accessor :edgecolor0, :edgecolor1
	attr_accessor :lanecolor0, :lanecolor1
	attr_reader :event_count
	def initialize(fname)
		@roadcolor0 = 0xFF000000
		@roadcolor1 = 0xFF000000
		@outcolor0 = 0xFF000000
		@outcolor1 = 0xFF000000
		@edgecolor0 = 0xFF000000
		@edgecolor1 = 0xFF000000
		@lanecolor0 = 0xFF000000
		@lanecolor1 = 0xFF000000
		@event_count = 0
		@events = []
		@fname = fname
	end
	def appendEvent(event, dist)
		newevent = event
		newevent.dist = dist
		@event_count = @event_count + 1
		@events.push(newevent)
	end
	def setRoadColor(color0, color1)
		@roadcolor0 = color0 | 0xFF000000
		@roadcolor1 = color1 | 0xFF000000
	end
	def setOutsideColor(color0, color1)
		@outcolor0 = color0 | 0xFF000000
		@outcolor1 = color1 | 0xFF000000
	end
	def setEdgeColor(color0, color1)
		@edgecolor0 = color0 | 0xFF000000
		@edgecolor1 = color1 | 0xFF000000
	end
	def setLaneColor(color0, color1)
		@lanecolor0 = color0 | 0xFF000000
		@lanecolor1 = color1 | 0xFF000000
	end
	def writeFile()
		outf = File.new(@fname, "w+")
		
		outf.write([ 0x0BADCAFE ].pack("N"))
		outf.write([ $HEADERSIZE ].pack("N"))
		outf.write([ @event_count ].pack("N"))
		outf.write([ $EVENTSIZE ].pack("N"))
		outf.write([ @roadcolor0 ].pack("N"))
		outf.write([ @roadcolor1 ].pack("N"))
		outf.write([ @outcolor0 ].pack("N"))
		outf.write([ @outcolor1 ].pack("N"))
		outf.write([ @edgecolor0 ].pack("N"))
		outf.write([ @edgecolor1 ].pack("N"))
		outf.write([ @lanecolor0 ].pack("N"))
		outf.write([ @lanecolor1 ].pack("N"))
# Write events
		events = @events
		while true
			event = events.shift
			break if !event
			event.writeToFile(outf)
		end
		outf.close
	end
end

