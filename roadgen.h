#ifndef ROADGEN_H_
#define ROADGEN_H_

#include <stdint.h>

#define ROADLINES 120
#define SPEED_MAX 50.0f

typedef struct {
	int active;
	float segment;
	float endsegment;
	float turn_pitch;
} Roadmotion;

typedef struct {
	int zmap[ROADLINES];
	float x, dx;
	float roadwidth;
	float edgewidth;
	float lanewidth;
	float laneoff;
	float texoff;
	float position;
	float speed;
	Roadmotion road_motions[8];
	float vstretch[ROADLINES];
	float xscroll;
	uint32_t roadcolors[2];
	uint32_t outcolors[2];
	uint32_t edgecolors[2];
	uint32_t lanecolors[2];
} Roadgen;

typedef void (*Roadraster_cb)(Roadgen*);

void roadgen_init(Roadgen* road);
void roadgen_draw(Roadgen* road, Roadraster_cb cb);

#endif

