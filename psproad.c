/*
 */
 
#include <pspkernel.h>
#include <pspdisplay.h>
#include <pspdebug.h>
#include <stdlib.h>
#include <pspctrl.h>
#include <pspgu.h>
#include <pspgum.h>

#include "callbacks.h"
#include "roadgen.h"
#include "video.h"
#include "trackload.h"

PSP_MODULE_INFO("PSPRoad Demo", 0, 1, 1);

#include "track.h"

extern unsigned int __attribute__((aligned(16))) gulist[262144];

int main(int argc, char* argv[])
{
	Roadgen road;
	Track track;
	Trackevent evs[4096];
	pspDebugScreenInit();
	setupCallbacks();
 
	initVideo();
	sceCtrlSetSamplingCycle(0);
	sceCtrlSetSamplingMode(PSP_CTRL_MODE_ANALOG);
	
	roadgen_init(&road);
	track.events = evs;
	track_load(track_bin, &track);
	track_register(track, &road);

	pspDebugScreenSetOffset((int)fbp0);
	pspDebugScreenSetXY(0,0);
	pspDebugScreenPrintf("Road inited\n");
	sceDisplayWaitVblankStart();
	fbp0 = sceGuSwapBuffers();
	sleep(1);

	while(running()) {
		SceCtrlData pad;
 
		sceGuStart(GU_DIRECT, gulist);
 
		sceGuClearColor(0xFFE06060);
		sceGuClearDepth(0);
		sceGuClear(GU_COLOR_BUFFER_BIT|GU_DEPTH_BUFFER_BIT);

		sceCtrlPeekBufferPositive(&pad, 1);

		if(pad.Buttons & PSP_CTRL_LEFT) {
			road.xscroll += 2.0f;
		}else if(pad.Buttons & PSP_CTRL_RIGHT) {
			road.xscroll -= 2.0f;
		}
		if(pad.Buttons & PSP_CTRL_UP) {
			road.speed += 1.0f / 16.0f;
		}else if(pad.Buttons & PSP_CTRL_DOWN) {
			road.speed -= 1.0f / 16.0f;
		}
		if(road.speed < 0) road.speed = 0;
		if(road.speed >= SPEED_MAX) road.speed = SPEED_MAX - 1;

		sceGumMatrixMode(GU_PROJECTION);
		sceGumLoadIdentity();
		sceGumOrtho(0, SCR_WIDTH, SCR_HEIGHT, 0, -1, 1);

		sceGumMatrixMode(GU_VIEW);
		sceGumLoadIdentity();
 
		sceGumMatrixMode(GU_MODEL);
		sceGumLoadIdentity();

                // Draw
		track_update_road(&road, &track);
		roadgen_draw(&road, NULL);

		sceGuFinish();
		sceGuSync(0,0);

		pspDebugScreenSetOffset((int)fbp0);
		pspDebugScreenSetXY(0,0);
		pspDebugScreenPrintf("Frame %.02f target %.02f en %d flags %08X",
			road.position, track.events[2].dist, track.events[2].enable,
			track.events[2].flags);

		sceDisplayWaitVblankStart();
		fbp0 = sceGuSwapBuffers();
	}
 
	sceGuTerm();

	sceKernelExitGame();
	return 0;
}
