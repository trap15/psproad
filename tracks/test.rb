#!/usr/bin/env ruby

unless ARGV.length == 1
	puts "Invalid arguments. Usage:"
	puts "	" + $0 + " track.bin"
	exit
end

require File.dirname(__FILE__) + "/core.rb"

track = Track.new(ARGV[0])
track.setRoadColor(0x808080, 0x606060)
track.setOutsideColor(0x40A010, 0x308008)
track.setEdgeColor(0xC0C0C0, 0x1040A0)
track.setLaneColor(0xC0C0C0, 0x606060)

event0 = TrackEvent.new(EventTypes::EVENT_CURVE)
event0.curveHandler(0)
event0.curvePitch(-0.02)
event0.curveIsRelative(0)
event0.curveSpeed(2.0)
track.appendEvent(event0, 5000)

event1 = TrackEvent.new(EventTypes::EVENT_CURVE)
event1.curveHandler(0)
event1.curvePitch(0.02)
event1.curveIsRelative(1)
event1.curveSpeed(1.0)
track.appendEvent(event1, 30000)

event2 = TrackEvent.new(EventTypes::EVENT_HILL)
event2.gradientLength(10000)
event2.gradientSpeed(1.5)
event2.gradientType(GradientTypes::GRADIENT_EXPONENT, 1.001)
track.appendEvent(event2, 60000)

track.writeFile()

